from django.shortcuts import render
from django.http import HttpResponse
from .models import Account

# Create your views here.
def index(request):
    return render(request, 'index.html', {})

def register_user(request):
	if request.method == 'POST':
		email = request.POST['email']
		name = request.POST['name']
		password = request.POST['password']

		Account.objects.create(
			email = email,
			name = name,
			password = password
		)

		return HttpResponse('')

def check_email(request):
    if request.method == "GET":
        raise Http404("URL doesn't exists")
    else:   
        response_data = {}
        email = request.POST["email"]
        user = None
        try:
            try:
                # we are matching the input again hardcoded value to avoid use of DB.
                # You can use DB and fetch value from table and proceed accordingly.
                for Account in Account:
                	if Account.email == email :
                		user = True
            except ObjectDoesNotExist as e:
                pass
            except Exception as e:
                raise e
            if not user:
                response_data["is_success"] = True
            else:
                response_data["is_success"] = False
        except Exception as e:
            response_data["is_success"] = False
            response_data["msg"] = "Some error occurred. Please let Admin know."

        return JsonResponse(response_data)
