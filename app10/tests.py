from django.test import TestCase
import unittest
from django.test import *
from django.urls import resolve
from .views import *
from .models import *

class MyTests(TestCase):
    
    def test_subs(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_template_hello(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_landing_page(self):
        response = Client().get('/')
        html_response = response.content.decode('utf-8')
        self.assertIn('Please Subscribe!',html_response)